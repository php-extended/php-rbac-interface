<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

use Stringable;

/**
 * UserStatusInterface interface file.
 * 
 * This represents a status for a user, meaning whether the user is valid and
 * active, meaning the logic of rbac can be used on this user.
 * 
 * @author Anastaszor
 */
interface UserStatusInterface extends Stringable
{
	
	/**
	 * Gets the identifier of this status.
	 * 
	 * @return string
	 */
	public function getIdentifier() : string;
	
	/**
	 * Gets whether this status is active.
	 * 
	 * @return boolean
	 */
	public function isActive() : bool;
	
}
