<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

use Throwable;

/**
 * UnprovidableThrowable interface file.
 * 
 * This represents an exception that is thrown when a problem occurs when
 * retrieving or understanding data from a provider.
 * 
 * @author Anastaszor
 */
interface UnprovidableThrowable extends Throwable
{
	
	// nothing special to add
	
}
