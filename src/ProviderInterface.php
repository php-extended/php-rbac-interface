<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

use Stringable;

/**
 * ProviderInterface interface file.
 * 
 * This represents the api from any storage component that is able to provide
 * users, groups, roles and rules.
 * 
 * @author Anastaszor
 */
interface ProviderInterface extends Stringable
{
	
	// {{{ User as anchor
	
	/**
	 * Gets the user with the given identifier, or null if it does not exist.
	 * 
	 * @param string $userId
	 * @return ?UserInterface
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getUser(string $userId) : ?UserInterface;
	
	/**
	 * Gets the groups the user is in, directly.
	 * 
	 * @param UserInterface $user
	 * @return array<integer, GroupInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getGroupsFromUser(UserInterface $user) : array;
	
	/**
	 * Gets the groups the user is in, recursively.
	 * 
	 * @param UserInterface $user
	 * @return array<integer, GroupInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getRecursiveGroupsFromUser(UserInterface $user) : array;
	
	/**
	 * Gets the roles the user has directly assigned.
	 * 
	 * @param UserInterface $user
	 * @return array<integer, RoleInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getRolesFromUser(UserInterface $user) : array;
	
	/**
	 * Gets the roles the user has assigned, or the roles that are children
	 * of those roles.
	 * 
	 * @param UserInterface $user
	 * @return array<integer, RoleInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getRecursiveRolesFromUser(UserInterface $user) : array;
	
	/**
	 * Gets the roles the user has via the groups in which it is, recursively,
	 * and the roles that are children of those roles.
	 * 
	 * @param UserInterface $user
	 * @return array<integer, RoleInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getRolesFromGroupsFromUser(UserInterface $user) : array;
	
	/**
	 * Gets the roles the user has, or has via the groups in which it is,
	 * recursively, and the roles that are children of those roles.
	 * 
	 * @param UserInterface $user
	 * @return array<integer, RoleInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getAllRolesFromUser(UserInterface $user) : array;
	
	// }}}
	
	// {{{ Group as anchor
	
	/**
	 * Gets the group with the given identifier, or null if it does not exist.
	 * 
	 * @param string $groupId
	 * @return ?GroupInterface
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getGroup(string $groupId) : ?GroupInterface;
	
	/**
	 * Gets the groups that are direct parents of the given group.
	 * 
	 * @param GroupInterface $group
	 * @return array<integer, GroupInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getParentGroups(GroupInterface $group) : array;
	
	/**
	 * Gets the groups that are parents of the given group, recursively.
	 * 
	 * @param GroupInterface $group
	 * @return array<integer, GroupInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getParentGroupsRecursive(GroupInterface $group) : array;
	
	/**
	 * Gets the groups that are direct children of the given group.
	 * 
	 * @param GroupInterface $group
	 * @return array<integer, GroupInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getChildrenGroups(GroupInterface $group) : array;
	
	/**
	 * Gets the groups that are children of the given group, recursively.
	 * 
	 * @param GroupInterface $group
	 * @return array<integer, GroupInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getChildrenGroupsRecursive(GroupInterface $group) : array;
	
	/**
	 * Gets the users that are affected to the given group.
	 * 
	 * @param GroupInterface $group
	 * @return array<integer, UserInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getUsersFromGroup(GroupInterface $group) : array;
	
	/**
	 * Gets the roles that are affected to the given group.
	 * 
	 * @param GroupInterface $group
	 * @return array<integer, RoleInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getRolesFromGroup(GroupInterface $group) : array;
	
	// }}}
	
	// {{{ Role as anchor
	
	/**
	 * Gets the role with the given identifier, or null if it does not exist.
	 * 
	 * @param string $roleId
	 * @return ?RoleInterface
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getRole(string $roleId) : ?RoleInterface;
	
	/**
	 * Gets all the roles that are parent of the given role, non recursively.
	 * 
	 * @param RoleInterface $role
	 * @return array<integer, RoleInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getParentRoles(RoleInterface $role) : array;
	
	/**
	 * Gets all the roles that are parents of the given role, recursively.
	 * 
	 * @param RoleInterface $role
	 * @return array<integer, RoleInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getParentRolesRecursive(RoleInterface $role) : array;
	
	/**
	 * Gets all the roles that are children of the given role, non recursively.
	 * 
	 * @param RoleInterface $role
	 * @return array<integer, RoleInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getChildrenRoles(RoleInterface $role) : array;
	
	/**
	 * Gets all the roles that are children of the given role, recursively.
	 * 
	 * @param RoleInterface $role
	 * @return array<integer, RoleInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getChildrenRolesRecursive(RoleInterface $role) : array;
	
	/**
	 * Gets all the groups that have the given role.
	 * 
	 * @param RoleInterface $role
	 * @return array<integer, GroupInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getGroupsFromRole(RoleInterface $role) : array;
	
	/**
	 * Gets all the users that have the given role.
	 * 
	 * @param RoleInterface $role
	 * @return array<integer, UserInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getUsersFromRole(RoleInterface $role) : array;
	
	/**
	 * Gets all the rules that belongs to the given role.
	 * 
	 * @param RoleInterface $role
	 * @return array<integer, RuleInterface>
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getRulesFromRole(RoleInterface $role) : array;
	
	// }}}
	
	/**
	 * Gets the rule with the given identifier, or null if it does not exist.
	 * 
	 * @param string $ruleId
	 * @return ?RuleInterface
	 * @throws UnprovidableThrowable if the data cannot be loaded
	 */
	public function getRule(string $ruleId) : ?RuleInterface;
	
}
