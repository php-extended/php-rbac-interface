<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

use Stringable;

/**
 * RegisterInterface interface file.
 * 
 * This represents the api to any storage component that is able to store users,
 * groups, roles and rules, and to make assignments.
 * 
 * @author Anastaszor
 */
interface RegisterInterface extends Stringable
{
	
	/**
	 * Saves the given user into persistant storage.
	 * 
	 * @param UserInterface $user
	 * @return boolean true if inserted, false if updated
	 * @throws UnregistrableThrowable if the user cannot be saved
	 */
	public function saveUser(UserInterface $user) : bool;
	
	/**
	 * Removes the user with the given user id from persistant storage. This
	 * also removes its assignments to roles and to groups.
	 * 
	 * @param string $userId
	 * @return boolean true if a user was removed, false if there were none
	 * @throws UnregistrableThrowable if any user cannot be deleted
	 */
	public function removeUser(string $userId) : bool;
	
	/**
	 * Assigns the given role to the given user.
	 * 
	 * @param string $roleId
	 * @param string $userId
	 * @return boolean true if inserted, false if updated
	 * @throws UnregistrableThrowable if no data can be deleted
	 */
	public function assignUser(string $roleId, string $userId) : bool;
	
	/**
	 * Revokes the role that was assigned to the given user.
	 * 
	 * @param string $roleId
	 * @param string $userId
	 * @return boolean true if a relation was removed, false if there were none
	 * @throws UnregistrableThrowable if no data can be deleted
	 */
	public function revokeUser(string $roleId, string $userId) : bool;
	
	/**
	 * Removes all the roles that are linked to the given user.
	 * 
	 * @param string $userId
	 * @return boolean true if a relation was removed, false if there were none
	 * @throws UnregistrableThrowable if no data can be deleted
	 */
	public function removeUserAllRoles(string $userId) : bool;
	
	/**
	 * Removes all the groups that are linked to the given user.
	 * 
	 * @param string $userId
	 * @return boolean true if a relation was removed, false if there were none
	 * @throws UnregistrableThrowable if no data can be deleted
	 */
	public function removeUserAllGroups(string $userId) : bool;
	
	/**
	 * Saves the given group into persistant storage.
	 * 
	 * @param GroupInterface $group
	 * @return boolean true if inserted, false if updated
	 * @throws UnregistrableThrowable if the group cannot be saved
	 */
	public function saveGroup(GroupInterface $group) : bool;
	
	/**
	 * Removes the group with the given group id from persistant storage. This
	 * also removes its assignmements to parent and children groups, its
	 * assignments to roles and to users.
	 * 
	 * @param string $groupId
	 * @return boolean true if a group was removed, false if there were none
	 * @throws UnregistrableThrowable if any group cannot be deleted
	 */
	public function removeGroup(string $groupId) : bool;
	
	/**
	 * Assigns the given role to the given group.
	 * 
	 * @param string $roleId
	 * @param string $groupId
	 * @return boolean true if inserted, false if updated
	 * @throws UnregistrableThrowable if no data can be deleted
	 */
	public function assignGroup(string $roleId, string $groupId) : bool;
	
	/**
	 * Revokes the given role that is attached to the given group.
	 * 
	 * @param string $roleId
	 * @param string $groupId
	 * @return boolean true if a relation was removed, false if there were none
	 * @throws UnregistrableThrowable if no data can be deleted
	 */
	public function revokeGroup(string $roleId, string $groupId) : bool;
	
	/**
	 * Sets the group with the child id to be a child of the group with the
	 * parent id.
	 * 
	 * @param string $parentId
	 * @param string $childId
	 * @return boolean true if inserted, false if updated
	 * @throws LoopThrowable if setting this relation would create a loop
	 * @throws UnregistrableThrowable if no relations cannot be saved
	 */
	public function setGroupChild(string $parentId, string $childId) : bool;
	
	/**
	 * Removes the relation that specifies the group with the child id to be a
	 * child of the group with the parent id.
	 * 
	 * @param string $parentId
	 * @param string $childId
	 * @return boolean true if a relation was removed, false if there were none
	 * @throws UnregistrableThrowable if no relations cannot be saved
	 */
	public function removeGroupChild(string $parentId, string $childId) : bool;
	
	/**
	 * Removes all the direct parents that are attached to the given group.
	 * 
	 * @param string $groupId
	 * @return boolean true if a relation was removed, false if there were none
	 * @throws UnregistrableThrowable if no data can be deleted
	 */
	public function removeGroupParents(string $groupId) : bool;
	
	/**
	 * Removes all the direct children that are attached to the given group.
	 * 
	 * @param string $groupId
	 * @return boolean true if a relation was removed, false if there were none
	 * @throws UnregistrableThrowable if no data can be deleted
	 */
	public function removeGroupChildren(string $groupId) : bool;
	
	/**
	 * Removes all the roles that are attached to the given group.
	 * 
	 * @param string $groupId
	 * @return boolean true if a relation was removed, false if there were none
	 * @throws UnregistrableThrowable if no data can be deleted
	 */
	public function removeGroupAllRoles(string $groupId) : bool;
	
	/**
	 * Removes all the users that are attached to the given group.
	 * 
	 * @param string $groupId
	 * @return boolean true if a relation was removed, false if there were none
	 * @throws UnregistrableThrowable if no data can be deleted
	 */
	public function removeGroupAllUsers(string $groupId) : bool;
	
	/**
	 * Saves the given role into persistant storage.
	 * 
	 * @param RoleInterface $role
	 * @return boolean true if inserted, false if updated
	 * @throws UnregistrableThrowable if the role cannot be saved
	 */
	public function saveRole(RoleInterface $role) : bool;
	
	/**
	 * Removes the role with the given role id from persistant storage. This
	 * also removes its assignments to parent and children roles, its
	 * assignments to roles and to users, and the afferent rules.
	 * 
	 * @param string $roleId
	 * @return boolean true if a role was removed, false if there were none
	 * @throws UnregistrableThrowable if no role cannot be deleted
	 */
	public function removeRole(string $roleId) : bool;
	
	/**
	 * Sets the role with the child id to be a child of the role with the
	 * parent id.
	 * 
	 * @param string $parentId
	 * @param string $childId
	 * @return boolean true if inserted, false if updated
	 * @throws LoopThrowable if setting this relation would create a loop
	 * @throws UnregistrableThrowable if no relations cannot be saved
	 */
	public function setRoleChild(string $parentId, string $childId) : bool;
	
	/**
	 * Removes the relation that specifies the group with the child id to be a
	 * child of the role with the parent id.
	 * 
	 * @param string $parentId
	 * @param string $childId
	 * @return boolean true if a relation was removed, false if there were none
	 * @throws UnregistrableThrowable if no relations cannot be saved
	 */
	public function removeRoleChild(string $parentId, string $childId) : bool;
	
	/**
	 * Removes all the direct parents that are attached to the given role.
	 * 
	 * @param string $roleId
	 * @return boolean true if a relation was removed, false if there were none
	 * @throws UnregistrableThrowable if no data can be deleted
	 */
	public function removeRoleParents(string $roleId) : bool;
	
	/**
	 * Removes all the direct children that are attached to the given role.
	 * 
	 * @param string $roleId
	 * @return boolean true if a relation was removed, false if there were none
	 * @throws UnregistrableThrowable if no data can be deleted
	 */
	public function removeRoleChildren(string $roleId) : bool;
	
	/**
	 * Removes all the groups that are attached to the given role.
	 * 
	 * @param string $roleId
	 * @return boolean true if a relation was removed, false if there were none
	 * @throws UnregistrableThrowable if no data can be deleted
	 */
	public function removeRoleAllGroups(string $roleId) : bool;
	
	/**
	 * Removes all the users that are attached to the given role.
	 * 
	 * @param string $roleId
	 * @return boolean true if a relation was removed, false if there were none
	 * @throws UnregistrableThrowable if no data can be deleted
	 */
	public function removeRoleAllUsers(string $roleId) : bool;
	
	/**
	 * Saves the given rule into persistant storage.
	 * 
	 * @param RuleInterface $rule
	 * @return boolean true if inserted, false if updated
	 * @throws UnregistrableThrowable if the group cannot be saved
	 */
	public function saveRule(RuleInterface $rule) : bool;
	
	/**
	 * Removes the rule with the given rule id from persistant storage.
	 * 
	 * @param string $ruleId
	 * @return boolean true if a rule was removed, false if there were none
	 * @throws UnregistrableThrowable if no rule cannot be deleted
	 */
	public function removeRule(string $ruleId) : bool;
	
}
