<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

use Stringable;

/**
 * UserInterface interface file.
 * 
 * This represents a user against which to check for access control.
 * 
 * @author Anastaszor
 */
interface UserInterface extends Stringable
{
	
	/**
	 * Gets the identifier of this user.
	 * 
	 * @return string
	 */
	public function getIdentifier() : string;
	
	/**
	 * Gets a string that is used to represent the user.
	 * 
	 * @return string
	 */
	public function getUsername() : string;
	
	/**
	 * Gets the status of this user.
	 * 
	 * @return UserStatusInterface
	 */
	public function getUserStatus() : UserStatusInterface;
	
	/**
	 * Gets the groups this user belongs to.
	 * 
	 * @return array<integer, GroupInterface>
	 */
	public function getAssignedGroups() : array;
	
	/**
	 * Gets the roles that this user has.
	 * 
	 * @return array<integer, RoleInterface>
	 */
	public function getAssignedRoles() : array;
	
}
