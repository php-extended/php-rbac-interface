<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

use Stringable;

/**
 * RuleInterface interface file.
 * 
 * This represents a rule that validates whether a specific user has a given
 * role with the given parameters.
 * 
 * @author Anastaszor
 */
interface RuleInterface extends Stringable
{
	
	/**
	 * Gets the identifier of this rule.
	 * 
	 * @return string
	 */
	public function getIdentifier() : string;
	
	/**
	 * Gets a string that is used to represent the rule.
	 * 
	 * @return string
	 */
	public function getRulename() : string;
	
	/**
	 * Gets the data that is inherent to the rule.
	 * 
	 * @return array<string, string>
	 */
	public function getData() : array;
	
	/**
	 * Validates whether this rule accepts the given user and role with given
	 * params.
	 * 
	 * @param UserInterface $user
	 * @param RoleInterface $role
	 * @param array<string, string> $params
	 * @return boolean true if the user is accepted in the role, false else
	 */
	public function validate(UserInterface $user, RoleInterface $role, array $params = []) : bool;
	
}
