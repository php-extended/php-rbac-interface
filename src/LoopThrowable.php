<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

use Throwable;

/**
 * LoopThrowable interface file.
 * 
 * This represents an exception that is thrown when a loop is detected when
 * setting hierarchy relations.
 * 
 * @author Anastaszor
 */
interface LoopThrowable extends Throwable
{
	
	// nothing special to add
	
}
