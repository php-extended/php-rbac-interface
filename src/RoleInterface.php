<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

use Stringable;

/**
 * RoleInterface interface file.
 * 
 * This represents a role that can be used to validate role based access control.
 * 
 * @author Anastaszor
 */
interface RoleInterface extends Stringable
{
	
	/**
	 * Gets the identifier of this group.
	 * 
	 * @return string
	 */
	public function getIdentifier() : string;
	
	/**
	 * Gets a string that is used to represent the role.
	 * 
	 * @return string
	 */
	public function getRolename() : string;
	
	/**
	 * Gets the status of this role.
	 * 
	 * @return RoleStatusInterface
	 */
	public function getRoleStatus() : RoleStatusInterface;
	
	/**
	 * Gets the rules that may be used to evaluate if this role applies.
	 * 
	 * @return array<integer, RuleInterface>
	 */
	public function getRules() : array;
	
	/**
	 * Gets the users that are assigned to this role.
	 * 
	 * @return array<integer, UserInterface>
	 */
	public function getAssignedUsers() : array;
	
	/**
	 * Gets the groups that are assigned to this role.
	 * 
	 * @return array<integer, GroupInterface>
	 */
	public function getAssignedGroups() : array;
	
	/**
	 * Gets the roles this role belongs to.
	 * 
	 * @return array<integer, RoleInterface>
	 */
	public function getParentRoles() : array;
	
	/**
	 * Gets the roles this role belongs to, recursively.
	 * 
	 * @return array<integer, RoleInterface>
	 */
	public function getParentRolesRecursive() : array;
	
	/**
	 * Gets the roles this role has as children.
	 * 
	 * @return array<integer, RoleInterface>
	 */
	public function getChildrenRoles() : array;
	
	/**
	 * Gets the roles this role has as children, recursively.
	 * 
	 * @return array<integer, RoleInterface>
	 */
	public function getChildrenRolesRecursive() : array;
	
}
