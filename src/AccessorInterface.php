<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

use Stringable;

/**
 * AccessorInterface interface file.
 * 
 * This interface represents an accessor that is able to decide given the data
 * and underlying persistant storage whether a user should be able to do the
 * action that is represented by the given role.
 * 
 * @author Anastaszor
 */
interface AccessorInterface extends Stringable
{
	
	/**
	 * Checks whether the given user is allowed to do the action represented by
	 * the given role assuming the given params.
	 * 
	 * @param string $userId
	 * @param string $roleId
	 * @param array<string, string> $params
	 * @return boolean true if the user is allowed, false if it is denied
	 * @throws UnprovidableThrowable if data cannot be retrieved from storage
	 */
	public function checkAccess(string $userId, string $roleId, array $params = []) : bool;
	
	/**
	 * If caching functions are implemented, clears cache data for the given user.
	 * 
	 * @param string $userId
	 * @return boolean true if caching data is now cleared for the user
	 */
	public function clearCacheForUser(string $userId) : bool;
	
	/**
	 * If caching functions are implemented, clears cache data for the given role.
	 * 
	 * @param string $roleId
	 * @return boolean true if caching data is now cleared for the role
	 */
	public function clearCacheForRole(string $roleId) : bool;
	
}
