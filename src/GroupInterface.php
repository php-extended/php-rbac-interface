<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

use Stringable;

/**
 * GroupInterface interface file.
 * 
 * This represents a group through which to check for access control.
 * 
 * @author Anastaszor
 */
interface GroupInterface extends Stringable
{
	
	/**
	 * Gets the identifier of this group.
	 * 
	 * @return string
	 */
	public function getIdentifier() : string;
	
	/**
	 * Gets a string that is used to represent the group.
	 * 
	 * @return string
	 */
	public function getGroupname() : string;
	
	/**
	 * Gets the status of this group.
	 * 
	 * @return GroupStatusInterface
	 */
	public function getGroupStatus() : GroupStatusInterface;
	
	/**
	 * Gets the users that this group has.
	 * 
	 * @return array<integer, UserInterface>
	 */
	public function getAssignedUsers() : array;
	
	/**
	 * Gets the roles that this group has.
	 * 
	 * @return array<integer, RoleInterface>
	 */
	public function getAssignedRoles() : array;
	
	/**
	 * Gets the groups this group belongs to.
	 * 
	 * @return array<integer, GroupInterface>
	 */
	public function getParentGroups() : array;
	
	/**
	 * Gets the groups this group belongs to, recursively.
	 * 
	 * @return array<integer, GroupInterface>
	 */
	public function getParentGroupsRecursive() : array;
	
	/**
	 * Gets the groups this group has as children.
	 * 
	 * @return array<integer, GroupInterface>
	 */
	public function getChildrenGroups() : array;
	
	/**
	 * Gets the groups this group has as children, recursively.
	 * 
	 * @return array<integer, GroupInterface>
	 */
	public function getChildrenGroupsRecursive() : array;
	
}
