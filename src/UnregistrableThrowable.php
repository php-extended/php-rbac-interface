<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

use Throwable;

/**
 * UnregistrableThrowable interface file.
 * 
 * This represents an exception that is thrown when a problem occurs when
 * saving any data from a register.
 * 
 * @author Anastaszor
 */
interface UnregistrableThrowable extends Throwable
{
	
	// nothing special to add
	
}
